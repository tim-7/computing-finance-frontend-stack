import React from 'react';
import logo from './logo.svg';
import './App.css';
import './components/home'

const App: React.FC = () => {
  return (
    <div className="App">
      <header className="App-header">
        <form className='form'>
          <div className='div-form'>
            <label className="input-label">
              Investasi Awal
            </label>
            <input className="input-form" type="number" name="invest" />
          </div>
          <div className='div-form'>
            <label className="input-label">
              Jangka Waktu (Bulan)
            </label>
            <input className="input-form" type="number" name="invest" />
          </div>
          <div className='div-form'>
            <label className="input-label">
              Target
            </label>
            <input className="input-form" type="number" name="invest" />
          </div>
        </form>
      </header>
    </div>
  );
}

export default App;
