## Tutorial singkat
1. Jalankan `git clone https://gitlab.com/tim-7/computing-finance-frontend-stack.git` untuk clone repo
2. Jalankan `yarn install` di dalam directory untuk menginstall dependecies
3. Jalankan `yarn start` untuk test apakah aplikasi berjalan

**Diharpakan menggunakan `yarn` sebagai package manager agar seragam dan mencegah hal hal yang tidak diinginkan**   

Dideploy ke:
http://tabungan-cuan.herokuapp.com/
